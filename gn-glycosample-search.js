import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import 'web-animations-js/web-animations-next.min.js';

/*
 gene, organism, disease, taxonomy
 class PageSearch extends PolymerElement {
 */
class GlycoSampleSearch extends PolymerElement {
  static get properties() {
    return {
      criteria: {
        type: String,
        notify: true,
      },
      label: {
        type: String,
      }

    };
  }
  // render() {
  static get template() {
    return html`
<style type="text/css">
</style>
<paper-input label="{{label}}" value="{{criteria}}"></paper-input>
`;

  }
}

customElements.define('gn-glycosample-search', GlycoSampleSearch);
