import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@google-web-components/google-chart';

/*
 gene, organism, disease, taxonomy
 */
class PageTable extends PolymerElement {

  static get properties() {
    return {
      resultdata: {
        notify: true,
        type: Object,
        value: function () {
          return {};
        }
      },
    };
  }

  static get template() {
    return html`<style type="text/css">
google-chart {
  width: 100%;
  height: 100%;
}
</style>

<google-chart options='{"allowHtml":true}' id="tablechart" type="table" rows='[[resultdata]]' cols='[{"label": "ID", "type": "string"},{"label": "InternalID", "type": "string"},{"label": "Title", "type": "string"},{"label": "Gene", "type": "string"},{"label": "Organism", "type": "string"},{"label": "Disease", "type": "string"},{"label": "DOID", "type": "string"},{"label": "Taxonomy", "type": "string"}]'></google-chart>
`;
  }
}

customElements.define('gn-glycosample-table', PageTable);
