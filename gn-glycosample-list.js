import {
  PolymerElement,
  html
} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';

import './gn-glycosample-menu.js';
import './gn-glycosample-limit.js';
import './gn-glycosample-disease-chart.js';
import './gn-glycosample-gene-chart.js';
import './gn-glycosample-organism-chart.js';
import './gn-glycosample-taxonomy-chart.js';
import './gn-glycosample-search.js';
// import './gn-glycosample-disease-search.js';
// import './gn-glycosample-gene-search.js';
// import './gn-glycosample-organism-search.js';
// import './gn-glycosample-taxonomy-search.js';
import './gn-glycosample-table.js';

class PageDemo extends PolymerElement {
  static get template() {
    return html `
<style type="text/css">
  #facet {
    /* display: block; */
    border: 1px
    solid red;
  }

  #table {
   /* display: block; */
   border: 1px
   solid green;
  }

  #chart0 {
    /* display: block; */
    border: 1px
    solid red;
  }

  #chart1 {
    /* display: block; */
    border: 1px
    solid purple;
  }

  #disease {
    float: left;
  }

  #gene {
    float: left;
  }
  #menu {
    float: left;
  }

  google-chart {
    height: 250px;
    width: 600px;
  }
}

</style>

<iron-ajax id="ajax" auto="" url="https://test.sparqlist.glyconavi.org/api/GlycoSampleList_v2_page_search_criteria?limit={{limit}}&offset={{offset}}&criteria={{sparqlcriteria}}&selectsubject={{selectsubject}}" handle-as="json" last-response="{{resultdata}}"></iron-ajax>

<div id="facet">
<div id="chart0">
<div id="disease">
<gn-glycosample-disease-chart selection="{{diseasecriteria}}" ></gn-glycosample-disease-chart>
<gn-glycosample-search criteria="{{diseasecriteria}}" label="Disease Search Criteria"</gn-glycosample-search>
</div>
<div id="organism">
<gn-glycosample-organism-chart selection="{{organismcriteria}}"></gn-glycosample-organism-chart>
<gn-glycosample-search criteria="{{organismcriteria}}" label="Organism Search Criteria"</gn-glycosample-search>
</div>
</div>
<div id="chart1">
<div id="gene">
<gn-glycosample-gene-chart selection="{{genecriteria}}"></gn-glycosample-gene-chart>
<gn-glycosample-search criteria="{{genecriteria}}" label="Gene Search Criteria"</gn-glycosample-search>
</div>
<div id="taxonomy">
<gn-glycosample-taxonomy-chart selection="{{taxonomycriteria}}"></gn-glycosample-taxonomy-chart>
<gn-glycosample-search criteria="{{taxonomycriteria}}" label="Taxonomy Search Criteria"</gn-glycosample-search>
</div>
</div>

<div id="table">
<div id="limit">
<gn-glycosample-limit limit="{{limit}}"></gn-glycosample-limit>
</div>
<div id="menu">
<gn-glycosample-menu page="{{page}}" limit="{{limit}}" offset="{{offset}}" criteria="{{sparqlcriteria}}"></gn-glycosample-menu>
</div>
<gn-glycosample-table resultdata="{{resultdata}}"></gn-glycosample-table>
</div>
`;
  }
  static get properties() {
    return {
      selection: {
        notify: true,
        type: Object,
        value: function() {
          return {};
        }
      },
      resultdata: {
        notify: true,
        type: Object,
        value: function() {
          return {};
        }
      },
      page: {
        type: String,
        notify: true,
        value: "1"
      },
      limit: {
        type: String,
        notify: true,
        value: "50"
      },
      offset: {
        type: String,
        notify: true,
        value: "0"
      },
      genecriteria: {
        type: String,
        notify: true,
        observer: '_geneChanged'
      },
      organismcriteria: {
        type: String,
        notify: true,
        observer: '_organismChanged'
      },
      diseasecriteria: {
        type: String,
        notify: true,
        observer: '_diseaseChanged'
      },
      taxonomycriteria: {
        type: String,
        notify: true,
        observer: '_taxonomyChanged'
      },
      sparqlcriteria: {
        type: String,
        notify: true,
        value: 'optional { ?gs gs:gene ?gene . } ' +
          'optional { ?gs gs:organism ?organism . } ' +
          'optional { ?gs gn:disease ?disease_res . ?disease_res  rdf:type sio:SIO_010299 . ?disease_res rdfs:label ?disease . ?disease_res gn:has_doid ?doid . } ' +
          'optional { ?gs gs:taxonomy ?taxonomy . } ',
      },
      // sparqlcriteria is a json mapping to control the where clause;
      sparqlcriteriaobject: {
        type: Object,
        notify: true,
        value: { gene: 'optional { ?gs gs:gene ?gene . } ', organism: 'optional { ?gs gs:organism ?organism . } ', disease: 'optional { ?gs gn:disease ?disease_res . ?disease_res  rdf:type sio:SIO_010299 . ?disease_res rdfs:label ?disease . ?disease_res gn:has_doid ?doid . } ', taxonomy: 'optional { ?gs gs:taxonomy ?taxonomy . } '},
      }
    };
  }
  _formatLimit(value) {
    console.log("formatLimit: " + value);
    var choice = "10";
    switch (value) {
      case 0:
        choice = "10";
        break;
      case 1:
        choice = "50";
        break;
      case 2:
        choice = "100";
        break;
    }
    return choice;
  }
  connectedCallback() {
    super.connectedCallback();
    console.log("cc");
  }
  handleDiseaseChange(e) {
    this.diseasecriteria = e.detail.value;
  }
  handleGeneChange(e) {
    this.genecriteria = e.detail.value;
  }
  handleTaxonomyChange(e) {
    this.taxonomycriteria = e.detail.value;
  }
  handleOrganismChange(e) {
    this.organismcriteria = e.detail.value;
  }
  // Observer method defined as a class method
  _diseaseChanged(newValue, oldValue) {
    this.chooseCriteria("disease", newValue);
  }
  _geneChanged(newValue, oldValue) {
    this.chooseCriteria("gene", newValue);
  }
  _organismChanged(newValue, oldValue) {
    this.chooseCriteria("organism", newValue);
  }
  _taxonomyChanged(newValue, oldValue) {
    this.chooseCriteria("taxonomy", newValue);
  }
  chooseCriteria(choice, newValue) {
    // console.log("chooseCriteria");
    // console.log(newValue);
    // console.log(choice);
    if ((newValue === null) || (newValue.length === 0)) {
      console.log("removed");
      switch (choice) {
        case "gene":
          this.sparqlcriteriaobject.gene = 'optional { ?gs gs:gene ?gene . } ';
          break;
        case "organism":
          this.sparqlcriteriaobject.organism = 'optional { ?gs gs:organism ?organism . } ';
          break;
        case "disease":
          this.sparqlcriteriaobject.disease = 'optional { ?gs gn:disease ?disease_res . ?disease_res  rdf:type sio:SIO_010299 . ?disease_res rdfs:label ?disease . ?disease_res gn:has_doid ?doid . } ';
          break;
        case "taxonomy":
          this.sparqlcriteriaobject.taxonomy = 'optional { ?gs gs:taxonomy ?taxonomy . } ';
          break;
        default:
          console.log("ERROR should never be in default");
      }
      // check criterias to reset
      // console.log("criterias");
      // console.log(this.genecriteria);
      // console.log(this.organismcriteria);
      // console.log(this.diseasecriteria);
      // console.log(this.taxonomycriteria);
      // if ((this.genecriteria === null || this.genecriteria.length === 0 ) && (this.organismcriteria === null || this.organismcriteria.length === 0) && (this.diseasecriteria === null || this.diseasecriteria.length === 0) && (this.taxonomycriteria === null || this.taxonomycriteria.length === 0)) {
        // console.log("all criterias blank");
      // } else {
        // console.log("!all criterias blank");
      // }
    } else {
      switch (choice) {
        case "gene":
          // console.log("gene was changed");
          this.sparqlcriteriaobject.gene = '?gs gs:gene ?gene . ' +
            'FILTER contains( str(?gene), "' + newValue + '") ';
          // ?gene bif:contains "' + newValue + '" .';
          break;
        case "organism":
          // console.log("organism was changed");
          this.sparqlcriteriaobject.organism = '?gs gs:organism ?organism . ' +
            'FILTER contains( str(?organism), "' + newValue + '") ';
          break;
        case "disease":
          // console.log("disease was changed");
          this.sparqlcriteriaobject.disease = '?gs gn:disease ?disease_res . ?disease_res  rdf:type sio:SIO_010299 . ?disease_res rdfs:label ?disease . ?disease_res gn:has_doid ?doid . ' +
            'FILTER contains( str(?disease), "' + newValue + '") ';
          // ?disease bif:contains "' + newValue + '" .';
          break;
        case "taxonomy":
          // console.log("taxon was changed");
          this.sparqlcriteriaobject.taxonomy = '?gs gn:disease ?disease_res . ?disease_res  rdf:type sio:SIO_010299 . ?disease_res rdfs:label ?disease . ?disease_res gn:has_doid ?doid . ' +
            'FILTER contains( str(?taxonomy), "' + newValue + '") ';
          // ?taxonomy bif:contains "' + newValue + '" .';
          break;
        default:
          console.log("ERROR should never be in default");
      }
    }
    this._parseSparqlCriteria();
  }


  _parseSparqlCriteria() {
    var crit = "";
    console.log(this.sparqlcriteriaobject);
    console.log(Object.keys(this.sparqlcriteriaobject));
    var keysarray = Object.keys(this.sparqlcriteriaobject);
    for (var i = 0; i < keysarray.length; i++) {
      var key = keysarray[i];
      console.log("key");
      console.log(key);
      console.log(this.sparqlcriteriaobject[key]);
      crit += this.sparqlcriteriaobject[key];
    }
    this.sparqlcriteria = crit;
    console.log(this.sparqlcriteria);
    this._reset();
  }

  _reset() {
    this.page=1;
    this.offset=0;
    this.limit=50;
  }
}

customElements.define('gn-glycosample-list', PageDemo);
